@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Jelovnik <a class="float-right" href="{{ route('home') }}">Početna</a></div>

                <div class="card-body">
                    
                    <p>Naziv jela: <br><b>{{ $task->naziv_jela}}</b></p>
                    <p>Naziv jela na engleskom: <br><b>{{ $task->naziv_jela_na_engleskom }}</b></p>
                    <p>Opis jela: <br><i>{{ $task->opis_jela }}</i></p>
                    <p>Tip jela: <br><b>{{ $task->tip_jela}}</b></p>


                     @if($task->gosti_id != null)

                        <p>Naručeno od: <br><b>{{ $task->gosti->name }} ({{ $task->gosti->email }})</b></p>

                    @else

                        @if (!isset($gosti) && $task->zaposlenici_id == Auth::id())
                            <p>Narudžbe</p>

                            <table>

                                <tbody>
                                    @foreach ($task->applications as $application)
                                    <tr>
                                        <td>
                                            {{ $application->name }} ({{ $application->email }}) (<b>Prioritet {{ $application->pivot->priority }}</b>)
                                        </td>
                                        <td>
                                            @if($application->pivot->priority == 1)
                                                <form method='post' action="{{ route('task.approve') }}">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="id" value="{{ $application->id }}">
                                                    <input type="hidden" name="task_id" value="{{ $task->id }}">
                                                    <button type="submit" class="btn btn-primary">Prihvati narudžbu</button>
                                                </form>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        @endif

                    @endif
    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Jelovnik <a class="float-right" href="{{ route('home') }}">Početna</a></div>

                <div class="card-body">
                    
                    <ul>
                        @foreach ($tasks as $task)
                            <li><a href="{{ route('gosti.preview', ['id' => $task->id]) }}">{{ $task->naziv_jela }}</a> 
                        @endforeach
                    </ul>


                    <p>Narudžbe</p>

                    <ul>
                        @if(count($on_tasks) > 0)
                            @foreach ($on_tasks as $task)
                                <li>{{ $task->naziv_jela }} (<b>Prioritet {{ $task->pivot->priority }}</b>)</li>
                            @endforeach
                        @else
                            Nema prijava
                        @endif
                    </ul>

                    @if(count($on_tasks) < 4)

                        <p>Prijave za narudžbe jela {{ (count($on_tasks)+1) }}</p>

                        <form method="post" action="{{ route('gosti.enroll') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="priority" value="{{ (count($on_tasks)+1) }}">

                            <div class="form-group">
                                <label for="task_id">Odaberite jelo</label>
                                <select class="form-control" name="task_id" id="task_id">
                                    @foreach ($tasks as $task)
                                       
                                        @if($task->gosti_id != null || $on_tasks->contains($task->id))
                                            {{-- Do Nothing --}}
                                        @else
                                            <option value="{{ $task->id }}">{{ $task->naziv_jela }}</option>
                                        @endif

                                    @endforeach
                                </select> 
                            </div>

                            <button type="submit" class="btn btn-primary">Naruči</button>
                        </form>

                    @endif

                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection